package ru.tsc.apozdnov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}